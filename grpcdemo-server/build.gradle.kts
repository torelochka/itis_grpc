@file:Suppress("DSL_SCOPE_VIOLATION", "UnstableApiUsage")

import java.time.LocalDateTime

println("Configuration phase executing, time: ${getDate()}")

plugins {
    alias(cslibs.plugins.kotlin)
}

allprojects {
    group = "ru.itis"
}

fun getDate() = LocalDateTime.now()

