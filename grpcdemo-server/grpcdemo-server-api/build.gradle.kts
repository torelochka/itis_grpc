@file:Suppress("DSL_SCOPE_VIOLATION")

import com.google.protobuf.gradle.id
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


plugins {
    kotlin("jvm")
    alias(cslibs.plugins.grpc.compiler)
    `maven-publish`
}

dependencies {
    implementation(cslibs.grpc.kotlin)
    implementation(cslibs.grpc.java)
    implementation(cslibs.protobuf.kotlin)
    implementation(cslibs.protobuf.java)
    implementation(cslibs.grpc.protobuf)

    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions:1.2.2")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:1.7.3")
}

sourceSets {
    val main by getting { }
    main.java.srcDirs("build/generated/source/proto/main/kotlin")
    main.java.srcDirs("build/generated/source/proto/main/grpckt")
}

tasks.withType<KotlinCompile> {
    dependsOn("generateProto")
}


protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:3.22.2"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:1.52.1"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:1.3.0:jdk8@jar"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                id("grpc")
                id("grpckt")
            }
            it.builtins {
                id("kotlin")
            }
        }
    }
}

repositories {
	mavenCentral()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "grpcdemo"
            artifactId = "grpcdemo-api"
            from(components["kotlin"])
        }
    }
    repositories {
        maven {
            url = uri("http://localhost:8081/nexus/content/repositories/grpc")
            credentials {
                username = "grpc_user"
                password = "Qwerty007"
            };
            isAllowInsecureProtocol = true
        }
    }
}