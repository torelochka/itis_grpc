rootProject.name = "grpcdemo-server"

include(
    "grpcdemo-server", "grpcdemo-server-api"
)

enableFeaturePreview("VERSION_CATALOGS")

dependencyResolutionManagement {
    repositories {
	mavenCentral()
    }
    versionCatalogs {
        create("cslibs") {
            from(files("gradle/cslibs.versions.toml"))
        }
    }
}
