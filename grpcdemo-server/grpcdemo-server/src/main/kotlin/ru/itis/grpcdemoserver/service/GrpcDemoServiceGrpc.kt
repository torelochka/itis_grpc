package ru.itis.grpcdemoserver.service

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import net.devh.boot.grpc.server.service.GrpcService
import ru.itis.grpcdemo.api.grpc.*
import java.util.UUID

@GrpcService
class GrpcDemoServiceGrpc : DemoServiceGrpcKt.DemoServiceCoroutineImplBase() {

    private val RED_COLOR = "\u001B[32m";
    private val ANSI_RESET = "\u001B[0m";

    override fun userMessages(requests: Flow<UserMessage>): Flow<ServerMessage> {
        return requests.map {userMessage ->
            println(userMessage)
            ServerMessage.newBuilder().setMessage("Processed: $userMessage").build()
        }
    }

    override suspend fun createUser(request: UserCreateRequest): UserCreateResponse {
        println("$RED_COLOR NAME: $ANSI_RESET ${request.name}")
        println("$RED_COLOR LASTNAME: $ANSI_RESET ${request.lastname}")
        println("$RED_COLOR MIDDLE_NAME: $ANSI_RESET ${request.patronymic}")
        println("$RED_COLOR FAV NUMBERS: $ANSI_RESET ${request.favNumbersList}")
        println("$RED_COLOR NUMBER: $ANSI_RESET ${request.someNumber}")
        println("$RED_COLOR OPTIONAL_NUMBER: $ANSI_RESET ${request.hasOptionalSomeNumber()}")
        println("$RED_COLOR LOGS: $ANSI_RESET ${request.logs}")
        println("$RED_COLOR GENDER: $ANSI_RESET ${request.gender}")

        println("-----------------------------")

        return UserCreateResponse.newBuilder().setId(UUID.randomUUID().toString()).build()
    }
}