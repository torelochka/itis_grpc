rootProject.name = "grpcdemo-client"

include(
    "grpcdemoclient"
)

enableFeaturePreview("VERSION_CATALOGS")

dependencyResolutionManagement {
    repositories {
	    mavenCentral()
    }
    versionCatalogs {
        create("cslibs") {
            from(files("gradle/cslibs.versions.toml"))
        }
    }
}
