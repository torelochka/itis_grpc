@file:Suppress("DSL_SCOPE_VIOLATION", "UnstableApiUsage")

plugins {
    kotlin("jvm")
    application

    alias(cslibs.plugins.kotlin.spring)
    alias(cslibs.plugins.spring.dependency.management)
    alias(cslibs.plugins.spring.boot)
    kotlin("kapt")
}

var exclusions = project.properties["test_exclusions"].toString()

tasks.test { useJUnitPlatform() }

dependencies {
    implementation("grpcdemo:grpcdemo-api:0.0.5")

    implementation(cslibs.spring.starter.webflux)
    implementation(cslibs.spring.starter.grpc)

    implementation(cslibs.grpc.kotlin)

    implementation(cslibs.kotlin.jackson)
    implementation(cslibs.kotlin.extensions)
    implementation(cslibs.kotlin.coroutines.reactor)
}
repositories {
mavenCentral()
    maven { isAllowInsecureProtocol = true; url = uri("http://localhost:8081/nexus/content/repositories/grpc") }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=all")
        jvmTarget = JavaVersion.VERSION_17.toString()
    }
}