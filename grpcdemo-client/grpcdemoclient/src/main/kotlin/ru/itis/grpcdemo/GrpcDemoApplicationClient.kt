package ru.itis.grpcdemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GrpcDemoApplicationClient

fun main(args: Array<String>) {
     runApplication<GrpcDemoApplicationClient>(*args)
}
