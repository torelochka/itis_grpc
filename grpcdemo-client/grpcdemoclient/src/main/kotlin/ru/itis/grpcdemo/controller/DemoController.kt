package ru.itis.grpcdemo.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.itis.grpcdemo.api.grpc.UserCreateResponse
import ru.itis.grpcdemo.service.GrpcDemoClient

@RestController
class DemoController(
        private val grpcDemoClient: GrpcDemoClient
) {

    @GetMapping("/test")
    suspend fun test(@RequestParam middleName: String): UserCreateResponse {
        return grpcDemoClient.userCreate(middleName)
    }
}