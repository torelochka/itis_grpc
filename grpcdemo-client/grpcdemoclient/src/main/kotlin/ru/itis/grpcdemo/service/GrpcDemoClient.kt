package ru.itis.grpcdemo.service

import net.devh.boot.grpc.client.inject.GrpcClient
import org.springframework.stereotype.Service
import ru.itis.grpcdemo.api.grpc.*

@Service
class GrpcDemoClient(
        @GrpcClient("grpc-demo")
        private var client: DemoServiceGrpcKt.DemoServiceCoroutineStub
) {

    suspend fun userCreate(middleName: String): UserCreateResponse {
        return client.createUser(UserCreateRequest.newBuilder().setMiddleName(middleName).build())
    }
}